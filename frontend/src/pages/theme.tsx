import {makeStyles} from "@material-ui/core/styles";

export const useHomeStyles = makeStyles(() => ({
    wrapper: {
        height: '100vh'
    },
    sideMenuList: {
        listStyle: 'none',
        padding: 0,
        margin: 0,
    },
    sideMenuListItem: {
        display: 'flex',
        alignItems: 'center',
    },
    sideMenuListItemBtn: {
        cursor: "pointer",
        marginTop: 20,
        color: 'white',
        padding: '10px 100px',
        border: 'none',
        backgroundColor: '#00acee',
        fontWeight: 600,
        borderRadius: 30,
    },
    sideMenuListLabel: {
        fontWeight: 700,
        fontSize: 20,
        marginLeft: 15,
    },
    tweetsWrapper: {
        height: '100%',
        borderRadius: 0,
        borderTop: 0,
        borderBottom: 0,
    },
    tweetsHeader: {
        borderLeft: 0,
        borderRight: 0,
        borderTop: 0,
        borderRadius: 0,
        padding: '20px 15px',
        fontWeight: 700,
    },
    tweet: {
        cursor: 'pointer',
        '& a': {
            textDecoration: "none",
            color: '#000000',
        },
        '&:hover': {
            backgroundColor: 'rgb(245, 248, 250)'
        }
    },
    tweetsUserName: {
        color: 'gray',
        fontWeight: 500,
        paddingLeft: 15,
    },
    tweetFooter: {
        display: 'flex',
        justifyContent: 'space-between',
        position: 'relative',
        left: '-15px',
    },
    addTwitForm: {
        border: 'none',
        outline: 'none',
    },
    addTwitFormBtns: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20,
        borderTop: '1px solid #ccc'
    },
    addTwitFormBtnAdd: {
        cursor: "pointer",
        marginTop: 20,
        color: 'white',
        padding: '10px 30px',
        border: 'none',
        backgroundColor: '#00acee',
        fontWeight: 600,
        borderRadius: 30,
    }
}));