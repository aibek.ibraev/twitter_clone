import React, {useState} from 'react';
import {CircularProgress, Container, Grid, Paper, Typography} from '@material-ui/core';
import SideMenu from "../components/SideMenu";
import AddTwitModal from "../components/Modals/AddTwitModal";
import {useDispatch, useSelector} from "react-redux";
import {fetchTags} from "../store/ducks/tags/actionCreators";
import {useHomeStyles} from './theme';
import SearchMenu from "../components/SearchMenu";
import {useParams} from "react-router-dom";
import {fetchTweet} from "../store/ducks/tweet/actionCreators";
import {selectIsTagsLoading, selectTweetData} from "../store/ducks/tweet/selectors";
import Tweet from "../components/Tweets/TweetItem";

const TweetDetail = ():React.ReactElement => {
    const classes = useHomeStyles();
    const dispatch = useDispatch();
    const params = useParams();
    const tweetData = useSelector(selectTweetData)
    const isLoading = useSelector(selectIsTagsLoading)
    const [openAddTwit, setOpenAddTwit] = useState<boolean>(false);
    console.log(tweetData, 'tweetData')
    const handleChangeAddTwit = () => {
        setOpenAddTwit(!openAddTwit)
    }

    React.useEffect(() => {
            // @ts-ignore
        dispatch(fetchTweet(params.id))
            dispatch(fetchTags())
    },[dispatch])

    return (
        <Container maxWidth='lg' className={classes.wrapper}>
            <Grid container spacing={3}>
                <Grid item xs={3}>
                    <SideMenu classes={classes} handleChangeAddTwit={handleChangeAddTwit}/>
                </Grid>
                <Grid item xs={6}>
                    <Paper
                        variant='outlined'
                        className={classes.tweetsWrapper}
                    >
                        <Paper variant='outlined'>
                            <Typography
                                variant={'h6'}
                                className={classes.tweetsHeader}
                            >
                                Твитнуть
                            </Typography>
                        </Paper>
                        {
                            !isLoading && tweetData
                                ?
                                    <Tweet
                                        classes={classes}
                                        {...tweetData}
                                    />
                                :
                                <CircularProgress/>
                        }
                    </Paper>
                </Grid>
                <Grid item xs={3}>
                    <SearchMenu classes={classes}/>
                </Grid>
            </Grid>
            <AddTwitModal
                classes={classes}
                openAddTwit={openAddTwit}
                handleChangeAddTwit={handleChangeAddTwit}
            />
        </Container>
    );
};

export default TweetDetail;