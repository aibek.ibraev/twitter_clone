import React, {useState} from 'react';
import {CircularProgress, Container, Grid, Paper, Typography} from '@material-ui/core';
import SideMenu from "../components/SideMenu";
import AddTwitModal from "../components/Modals/AddTwitModal";
import {useDispatch, useSelector} from "react-redux";
import {fetchTweets} from "../store/ducks/tweets/actionCreators";
import {fetchTags} from "../store/ducks/tags/actionCreators";
import {useHomeStyles} from './theme';
import SearchMenu from "../components/SearchMenu";
import AddTwitForm from "../components/AddTwitForm";
import Tweet from "../components/Tweets/TweetItem";
import {selectIsTweetLoaded, selectIsTweetLoading, selectTweetsItems} from "../store/ducks/tweets/selectors";

const Home = ():React.ReactElement => {
    const classes = useHomeStyles();
    const dispatch = useDispatch();

    const [openAddTwit, setOpenAddTwit] = useState<boolean>(false);

    const handleChangeAddTwit = () => {
        setOpenAddTwit(!openAddTwit)
    }

    const tweets = useSelector(selectTweetsItems)
    const isLoading = useSelector(selectIsTweetLoading)
    const isLoaded = useSelector(selectIsTweetLoaded)

    React.useEffect(() => {
        dispatch(fetchTweets())
        dispatch(fetchTags())
    },[dispatch])

    return (
        <Container maxWidth='lg' className={classes.wrapper}>
            <Grid container spacing={3}>
                <Grid item xs={3}>
                    <SideMenu classes={classes} handleChangeAddTwit={handleChangeAddTwit}/>
                </Grid>
                <Grid item xs={6}>
                    <Paper
                        variant='outlined'
                        className={classes.tweetsWrapper}
                    >
                        <Paper variant='outlined'>
                            <Typography
                                variant={'h6'}
                                className={classes.tweetsHeader}
                            >
                                Главная
                            </Typography>
                        </Paper>
                        <AddTwitForm
                            classes={classes}
                            user={{
                                fullName: 'Aibek',
                                userName: 'i5raev',
                                avatarUrl: 'https://www.w3schools.com/w3images/avatar2.png',
                            }}
                        />
                        {
                            !isLoading ?
                                tweets.map(tweet => (
                                    <Tweet
                                        classes={classes}
                                        {...tweet}
                                    />
                                ))
                                :
                                <CircularProgress/>
                        }
                    </Paper>
                </Grid>
                <Grid item xs={3}>
                   <SearchMenu classes={classes}/>
                </Grid>
            </Grid>
           <AddTwitModal
               classes={classes}
               openAddTwit={openAddTwit}
               handleChangeAddTwit={handleChangeAddTwit}
           />
        </Container>
    );
};

export default Home;