import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Button, Typography} from "@mui/material";
import TwitterIcon from '@mui/icons-material/Twitter';
import SearchIcon from '@mui/icons-material/Search';
import PeopleOutline from '@mui/icons-material/PeopleOutline';
import ChatBubbleOutlineIcon from '@mui/icons-material/ChatBubbleOutline';
import SignInModal from "../components/Modals/SignInModal";
import SignUpModal from "../components/Modals/SignUpModal";

const useStyles = makeStyles((theme) => ({
    wrapper: {
        display: 'flex',
        height: '100vh',
    },
    blueSide: {
        backgroundColor: '#71C9F8',
        height: '100vh',
        flex: '0 0 50%',
        display: "flex",
        justifyContent: 'center',
        alignItems: 'center',
        overflow: "hidden",
        position: 'relative',
    },
    blueSideBigIcon: {
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
        color: '#00acee',
    },
    blueSideListInfo: {
        listStyle: 'none',
        margin: 0,
        padding: 0,
        width: 380,
        position: 'relative',
        '& h6': {
            color: 'white',
            fontWeight: '600',
            fontSize: 20,
            display: 'flex',
            alignItems: 'center',
        }
    },
    blueSideListInfoItem: {
        marginBottom: 40,
    },
    blueSideListInfoIcon: {
        fontSize: 32,
        marginRight: 20,
    },
    loginSide: {
        flex: '0 0 50%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginSideWrapper: {
        width: 380,
    },
    loginSideTwitterIcon: {
        fontSize: 45,
        color: '#00acee',
        marginBottom: 40,
    },
    loginSideTitle: {
        color: '#010000',
        fontWeight: 800,
        fontSize: 32,
        paddingBottom: 20,
    },
    loginSideButton: {
        paddingTop: 20,
    }
}));

export const SignIn: React.FC = (): React.ReactElement => {
    const classes = useStyles();
    const [openSignIn, setOpenSignIn] = useState<boolean>(false);
    const [openSignUp, setOpenSignUp] = useState<boolean>(false);

    const handleChangeSignIn = () => {
        setOpenSignIn(!openSignIn)
    }
    const handleChangeSignUp = () => {
        setOpenSignUp(!openSignUp)
    }

    return (
        <div className={classes.wrapper}>
            <section className={classes.blueSide}>
                <TwitterIcon className={classes.blueSideBigIcon} style={{ width: '300%', height: '300%' }}/>
                <ul className={classes.blueSideListInfo}>
                    <li className={classes.blueSideListInfoItem}>
                        <Typography variant='h6'>
                            <SearchIcon className={classes.blueSideListInfoIcon} style={{ fontSize: 32 }}/>
                            Читайте о том , что вам интересно.
                        </Typography></li>
                    <li className={classes.blueSideListInfoItem}>
                        <Typography variant='h6'>
                            <PeopleOutline className={classes.blueSideListInfoIcon} style={{ fontSize: 32 }}/>
                            Узнайте , о чем говорят в мире.
                        </Typography>
                    </li>
                    <li className={classes.blueSideListInfoItem}>
                        <Typography variant='h6'>
                            <ChatBubbleOutlineIcon className={classes.blueSideListInfoIcon} style={{ fontSize: 32 }}/>
                            Присоединяйтесь к общению.
                        </Typography>
                    </li>
                </ul>
            </section>
            <section className={classes.loginSide} color="primary">
               <div className={classes.loginSideWrapper}>
                   <TwitterIcon className={classes.loginSideTwitterIcon}  style={{ fontSize: 45 }}/>
                   <Typography className={classes.loginSideTitle}>
                       Узнайте , что происходит в мире прямо сейчас
                   </Typography>
                   <Typography className={classes.loginSideTitle}>
                       Присоединяйтесь к Твиттеру прямо сейчас!
                   </Typography>
                   <Button
                       variant='contained'
                       className={classes.loginSideButton}
                       fullWidth
                       onClick={handleChangeSignUp}
                       style={{
                           backgroundColor: '#00acee',
                           marginRight: '30px',
                           fontWeight: 600,
                           color: 'white',
                           borderRadius: '30px',
                           marginBottom: '20px'
                       }}
                   >
                       Зарегистрироваться
                   </Button>
                   <Button
                       variant='outlined'
                       className={classes.loginSideButton}
                       onClick={handleChangeSignIn}
                       fullWidth
                       style={{
                           color: '#00acee',
                           marginRight: '30px',
                           fontWeight: 600,
                           backgroundColor: 'white',
                           borderRadius: '30px',
                           marginBottom: '20px'
                       }}
                   >
                       Войти
                   </Button>
               </div>
            </section>
            <SignInModal
                openSignIn={openSignIn}
                handleChangeSignIn={handleChangeSignIn}
            />
            <SignUpModal
                openSignUp={openSignUp}
                handleChangeSignUp={handleChangeSignUp}
            />
        </div>
    );
}

