import React from 'react';
import {Avatar, Container, Grid, IconButton, Paper, TextField, Typography} from '@material-ui/core';
import ChatBubbleOutlineIcon from "@mui/icons-material/ChatBubbleOutline";
import RepeatIcon from "@mui/icons-material/Repeat";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import ReplyIcon from "@mui/icons-material/Reply";
import {Link} from "react-router-dom";
import {useHomeStyles} from "../../pages/theme";

interface TweetProps{
    id: string;
    classes: ReturnType<typeof useHomeStyles>;
    text: string;
    user: {
        fullname: string;
        username: string;
        avatarUrl: string,
    }
}

const Tweet: React.FC<TweetProps> = ({classes,user, text,id}: TweetProps): React.ReactElement => {
    return (
                <Paper variant='outlined' className={classes.tweet}>
                    <Link to={`/tweets/${id}`}>
                    <Grid container spacing={3} style={{
                        padding: 15,
                    }}>
                        <Grid item xs={1}>
                            <Avatar
                                src={user.avatarUrl}
                                alt={user.username}
                            />
                        </Grid>
                        <Grid item xs={11}>
                            <Typography>
                                <b>{user.fullname}</b>
                                <span className={classes.tweetsUserName}>@{user.username}</span>
                                <span className={classes.tweetsUserName}>1 ч</span>
                            </Typography>
                            <Typography variant={'body1'}>
                                <p>{text}</p>
                            </Typography>
                            <div className={classes.tweetFooter}>
                                <IconButton>
                                    <ChatBubbleOutlineIcon style={{fontSize:20, marginRight: 5}} color={'primary'}/>
                                    <span style={{fontSize: 15}}>1</span>
                                </IconButton>
                                <IconButton>
                                    <RepeatIcon style={{fontSize:20, marginRight: 5}}  color={'primary'}/>
                                    <span style={{fontSize: 15}}>1</span>
                                </IconButton>
                                <IconButton>
                                    <FavoriteBorderIcon style={{fontSize:20, marginRight: 5}}  color={'primary'}/>
                                    <span style={{fontSize: 15}}>1</span>
                                </IconButton>
                                <IconButton>
                                    <ReplyIcon style={{fontSize:20, marginRight: 5}}  color={'primary'}/>
                                    <span style={{fontSize: 15}}>1</span>
                                </IconButton>
                            </div>
                        </Grid>
                    </Grid>
                    </Link>
                </Paper>
    );
};

export default Tweet;