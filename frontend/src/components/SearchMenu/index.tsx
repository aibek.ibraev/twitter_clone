import React from 'react';
import {useHomeStyles} from "../../pages/theme";
import {TextField} from "@material-ui/core";
import Tags from "../Tags";

interface SearchMenuProps {
    classes: ReturnType<typeof useHomeStyles>;
}

const SearchMenu:React.FC<SearchMenuProps> = ({classes}:SearchMenuProps) => {
    return (
        <>
            <TextField label={'Поиск в Твиттере'} fullWidth/>
            <Tags classes={classes}/>
        </>
    );
};

export default SearchMenu;