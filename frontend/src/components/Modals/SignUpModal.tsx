import React from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle, TextField,
    Typography
} from "@material-ui/core";

interface SignUpModalProps {
    openSignUp: boolean;
    handleChangeSignUp: () => void;
}

const SignUpModal:React.FC<SignUpModalProps> = ({openSignUp,handleChangeSignUp}: SignUpModalProps) => {
    return (
        <Dialog
            open={openSignUp}
            onClose={handleChangeSignUp}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogContent
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    width: '500px',
                    padding: '30px 40px'
                }}
            >
                <Typography
                    gutterBottom
                    variant={'h6'}
                    style={{
                        textAlign: 'center'
                    }}
                >
                    {"Создайте учетную запись"}
                </Typography>
                <TextField
                    type={"text"}
                    id="firstName"
                    label="First Name"
                    variant="standard"
                    autoFocus
                    style={{
                        width: '100%',
                        marginBottom: '20px',
                    }}
                />
                <TextField
                    type={"text"}
                    id="lastName"
                    label="Last Name"
                    variant="standard"
                    autoFocus
                    style={{
                        width: '100%',
                        marginBottom: '20px',
                    }}
                />
                <TextField
                    type={"email"}
                    id="email"
                    label="E-mail"
                    variant="standard"
                    autoFocus
                    style={{
                        width: '100%',
                        marginBottom: '20px',
                    }}
                />
                <TextField
                    type={'password'}
                    id="password"
                    label="Password"
                    variant="standard"
                    autoFocus
                    style={{
                        width: '100%'
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={handleChangeSignUp}
                    style={{
                        color: '#00acee',
                        marginRight: '30px',
                        fontWeight: 600,
                    }}
                >Назад</Button>
                <Button
                    onClick={handleChangeSignUp}
                    autoFocus
                    style={{
                        color: 'white',
                        fontWeight: 600,
                        padding: '10px 20px',
                        marginRight: '30px',
                        borderRadius: '30px',
                        backgroundColor: '#00acee',
                    }}
                >Зарегистрироваться</Button>
            </DialogActions>
        </Dialog>
    );
};

export default SignUpModal;