import React from 'react';
import {
    Dialog,
    DialogContent,
     Typography
} from "@material-ui/core";
import AddTwitForm from "../AddTwitForm";
import {useHomeStyles} from "../../pages/theme";

interface AddTwitModalProps {
    classes: ReturnType<typeof useHomeStyles>;
    openAddTwit: boolean;
    handleChangeAddTwit: () => void;
}

const AddTwitModal:React.FC<AddTwitModalProps> = ({classes,openAddTwit,handleChangeAddTwit}:AddTwitModalProps) => {

    return (
        <Dialog
            open={openAddTwit}
            onClose={handleChangeAddTwit}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogContent style={{
                display: 'flex',
                flexDirection: 'column',
                width: '500px',
                padding: '30px 40px'
            }}>
                <Typography
                    gutterBottom
                    variant={'h6'}
                    style={{
                        textAlign: 'center'
                    }}
                >
                    {"Добавить твитт"}
                </Typography>
                <AddTwitForm
                    classes={classes}
                    user={{
                        fullName: 'Aibek',
                        userName: 'i5raev',
                        avatarUrl: 'https://www.w3schools.com/w3images/avatar2.png',
                    }}
                />
            </DialogContent>

        </Dialog>
    );
};

export default AddTwitModal;