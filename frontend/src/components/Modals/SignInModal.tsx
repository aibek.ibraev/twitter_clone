import React from 'react';
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    TextField, Typography
} from "@material-ui/core";

interface SignInModalProps {
    openSignIn: boolean;
    handleChangeSignIn: () => void;
}

const SignInModal:React.FC<SignInModalProps> = ({openSignIn,handleChangeSignIn}:SignInModalProps) => {
    return (
        <Dialog
            open={openSignIn}
            onClose={handleChangeSignIn}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogContent style={{
                display: 'flex',
                flexDirection: 'column',
                width: '500px',
                padding: '30px 40px'
            }}>
                <Typography
                    gutterBottom
                    variant={'h6'}
                    style={{
                        textAlign: 'center'
                    }}
                >
                    {"Войти в Twitter"}
                </Typography>
                <TextField
                    type={"email"}
                    id="email"
                    label="E-mail"
                    variant="standard"
                    autoFocus
                    style={{
                        width: '100%',
                        marginBottom: '20px',
                    }}
                />
                <TextField
                    type={'password'}
                    id="password"
                    label="Password"
                    variant="standard"
                    autoFocus
                    style={{
                        width: '100%'
                    }}
                />
            </DialogContent>
            <DialogActions>
                <Button
                    onClick={handleChangeSignIn}
                    style={{
                        color: '#00acee',
                        marginRight: '30px',
                        fontWeight: 600,
                    }}
                >
                    Назад
                </Button>
                <Button
                    onClick={handleChangeSignIn}
                    autoFocus
                    style={{
                        color: 'white',
                        fontWeight: 600,
                        padding: '10px 20px',
                        marginRight: '30px',
                        borderRadius: '30px',
                        backgroundColor: '#00acee',
                    }}
                >
                    Войти
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default SignInModal;