import React, {useState, useEffect} from 'react';
import {Avatar, CircularProgress, Grid, IconButton, Paper, Snackbar, TextareaAutosize} from "@material-ui/core";
import InsertPhotoIcon from '@mui/icons-material/InsertPhoto';
import GifBoxIcon from '@mui/icons-material/GifBox';
import SentimentSatisfiedAltIcon from '@mui/icons-material/SentimentSatisfiedAlt';
import EventNoteIcon from '@mui/icons-material/EventNote';
import {useDispatch, useSelector} from "react-redux";
import {fetchAddTweets} from "../../store/ducks/tweets/actionCreators";
import {selectAddFormState} from "../../store/ducks/tweets/selectors";
import {AddFormState} from "../../store/ducks/tweets/contracts/state";
import {useHomeStyles} from "../../pages/theme";

interface AddTwitFormProps{
    classes: ReturnType<typeof useHomeStyles>;
    user: {
        fullName: string;
        userName: string;
        avatarUrl: string,
    }
}
const AddTwitForm: React.FC<AddTwitFormProps> = ({classes,user}: AddTwitFormProps): React.ReactElement => {
    const dispatch = useDispatch();
    const addFormState = useSelector(selectAddFormState);

    const [text,setText] = useState<string>('');
    const [visibleNotification,setVisibleNotification] = useState<boolean>(false);

    useEffect(() => {
        if(addFormState === AddFormState.ERROR){
            setVisibleNotification(true)
        }else{
            setVisibleNotification(false)
        }
    },[addFormState])

    const handleClickAddTweet = () : void => {
        dispatch(fetchAddTweets(text))
        setText('')
    }

    const handleCloseNotification = () => {
        setVisibleNotification(false)
    }


    return (
        <Paper variant='outlined'>
            <Snackbar
                open={visibleNotification}
                onClose={handleCloseNotification}
                message={'Ошибка при добавлении твита :('}
                key={"topright"}
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
            />
            <Grid container spacing={3} style={{
                padding: 15,
            }}>
                <Grid item xs={1}>
                    <Avatar
                        src={user.avatarUrl}
                        alt={user.userName}
                    />
                </Grid>
                <Grid item xs={11}>
                    <TextareaAutosize
                        aria-label="Добавить твит"
                        placeholder="Добавить твит"
                        style={{ width: '100%' }}
                        className={classes.addTwitForm}
                        onChange={e => setText(e.target.value)}
                    />
                    <div className={classes.addTwitFormBtns}>
                        <div>
                            <IconButton>
                                <InsertPhotoIcon style={{fontSize:20, marginRight: 5}} color={'primary'}/>
                            </IconButton>
                            <IconButton>
                                <GifBoxIcon style={{fontSize:20, marginRight: 5}} color={'primary'}/>
                            </IconButton>
                            <IconButton>
                                <SentimentSatisfiedAltIcon style={{fontSize:20, marginRight: 5}} color={'primary'}/>
                            </IconButton>
                            <IconButton>
                                <EventNoteIcon style={{fontSize:20, marginRight: 5}} color={'primary'}/>
                            </IconButton>
                        </div>
                        <div>
                            <button
                                disabled={text.length <= 0}
                                className={classes.addTwitFormBtnAdd}
                                onClick={handleClickAddTweet}
                            >
                                {
                                    addFormState === AddFormState.LOADING
                                        ?
                                        <CircularProgress
                                            color="inherit"
                                            size={12}
                                        />
                                        :
                                        'Твитнуть'
                                }
                            </button>
                        </div>
                    </div>
                </Grid>
            </Grid>
        </Paper>
    );
};

export default AddTwitForm;