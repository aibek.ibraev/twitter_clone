import React from 'react';
import {CircularProgress} from "@material-ui/core";
import {useSelector} from "react-redux";
import {selectIsTagsLoaded, selectTagsItems} from "../../store/ducks/tags/selectors";
import {useHomeStyles} from "../../pages/theme";

interface TagsProps{
    classes: ReturnType<typeof useHomeStyles>;
}

const Tags:React.FC<TagsProps> = ({classes}: TagsProps): React.ReactElement | null => {
    const tags = useSelector(selectTagsItems)
    const isLoaded = useSelector(selectIsTagsLoaded)

    if(!isLoaded){
        return <CircularProgress/>;
    }

    return (
        <ul>
            {
                tags.map((tag, index) => (
                    <li key={index}>
                        {tag.name}
                    </li>
                ))
            }
        </ul>
    );
};

export default Tags;