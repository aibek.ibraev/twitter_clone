import React from 'react';
import {Grid, IconButton, Typography} from "@material-ui/core";
import TwitterIcon from "@mui/icons-material/Twitter";
import SearchIcon from "@mui/icons-material/Search";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import BookmarkBorderIcon from "@mui/icons-material/BookmarkBorder";
import ListAltIcon from "@mui/icons-material/ListAlt";
import PersonIcon from "@mui/icons-material/Person";
import {useHomeStyles} from "../../pages/theme";

interface SideMenuProps{
    classes: ReturnType<typeof useHomeStyles>;
    handleChangeAddTwit: () => void;
}

const SideMenu:React.FC<SideMenuProps> = ({classes,handleChangeAddTwit}: SideMenuProps): React.ReactElement => {
    return (
            <ul className={classes.sideMenuList}>
                <li className={classes.sideMenuListItem}>
                    <IconButton color={'primary'}>
                        <TwitterIcon  style={{ fontSize: 38, margin: '15px 0' }}/>
                    </IconButton>
                </li>
                <li className={classes.sideMenuListItem}>
                    <IconButton color={'primary'}>
                        <SearchIcon style={{ fontSize: 30 }}/>
                    </IconButton>
                    <Typography className={classes.sideMenuListLabel}>
                        Поиск
                    </Typography>
                </li>
                <li className={classes.sideMenuListItem}>
                    <IconButton color={'primary'}>
                        <NotificationsNoneIcon style={{ fontSize: 30 }}/>
                    </IconButton>
                    <Typography className={classes.sideMenuListLabel}>
                        Уведомления
                    </Typography>
                </li>
                <li className={classes.sideMenuListItem}>
                    <IconButton color={'primary'}>
                        <MailOutlineIcon style={{ fontSize: 30 }}/>
                    </IconButton>
                    <Typography className={classes.sideMenuListLabel}>
                        Сообщения
                    </Typography>
                </li>
                <li className={classes.sideMenuListItem}>
                    <IconButton color={'primary'}>
                        <BookmarkBorderIcon style={{ fontSize: 30 }}/>
                    </IconButton>
                    <Typography className={classes.sideMenuListLabel}>
                        Закладки
                    </Typography>
                </li>
                <li className={classes.sideMenuListItem}>
                    <IconButton color={'primary'}>
                        <ListAltIcon style={{ fontSize: 30 }}/>
                    </IconButton>
                    <Typography className={classes.sideMenuListLabel}>
                        Список
                    </Typography>
                </li>
                <li className={classes.sideMenuListItem}>
                    <IconButton color={'primary'}>
                        <PersonIcon style={{ fontSize: 30 }}/>
                    </IconButton>
                    <Typography className={classes.sideMenuListLabel}>
                        Профиль
                    </Typography>
                </li>
                <li className={classes.sideMenuListItem}>
                    <button
                        className={classes.sideMenuListItemBtn}
                        onClick={handleChangeAddTwit}
                    >
                        Твитнуть
                    </button>
                </li>
            </ul>
    );
};

export default SideMenu;