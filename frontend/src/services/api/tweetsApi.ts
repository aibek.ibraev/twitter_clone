import axios from "axios";
import {Tweet, TweetState} from "../../store/ducks/tweets/contracts/state";

export const TweetsApi = {
    fetchTweets (): Promise<TweetState['items']> {
        return axios.get('http://localhost:3000/tweets').then(({data}) => data)
    },
    fetchTweetData (id: string): Promise<Tweet> {
        return axios.get('http://localhost:3000/tweets/' + id).then(({data}) => data)
    },
    addTweets (payload: Tweet): Promise<Tweet> {
        return axios.post('http://localhost:3000/tweets', payload).then(({data}) => data)
    },
}
