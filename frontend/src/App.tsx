import React from 'react';
import {SignIn} from "./pages/SignIn";
import Home from "./pages/Home";
import {Route, Switch} from 'react-router-dom';
import TweetDetail from "./pages/TweetDetail";

function App() {
  return (
    <div className="App">
        <Switch>
            <Route path='/signin' component={SignIn} exact/>
            <Route path='/' component={Home} exact/>
            <Route path='/tweets/:id' component={TweetDetail} exact/>
        </Switch>
    </div>
  );
}

export default App;
