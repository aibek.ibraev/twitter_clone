import {call, put, takeLatest} from 'redux-saga/effects'
import {setTweetData, setTweetLoadingState} from "./actionCreators";
import {LoadingState} from "./contracts/state";
import {DataTweetActionsType, FetchTweetActionInterface} from "./contracts/actionTypes";
import {TweetsApi} from "../../../services/api/tweetsApi";

export function* fetchTweetDataRequest({payload: tweetId}: FetchTweetActionInterface) {
    try {
        // @ts-ignore
        const data = yield call(TweetsApi.fetchTweetData, tweetId);
        yield put(setTweetData(data));
    }catch (error){
        yield put(setTweetLoadingState(LoadingState.ERROR));
        console.log(error)
    }
}


export function* tweetDataSaga() {
    // @ts-ignore
    yield takeLatest(DataTweetActionsType.FETCH_TWEET_DATA, fetchTweetDataRequest)
}