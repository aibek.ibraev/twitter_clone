import { createSelector } from 'reselect';
import {RootState} from "../../store";
import {DataTweetState, LoadingState} from "./contracts/state";

export const selectTweet = (state: RootState): DataTweetState => state.tweet;

export const selectTweetData = (state: RootState): DataTweetState['data'] => selectTweet(state).data;

export const selectLoadingState = (state: RootState) : LoadingState => selectTweet(state).loadingState;

export const selectIsTagsLoading = (state: RootState) : boolean => selectLoadingState(state) === LoadingState.LOADING;

export const selectIsTagsLoaded = (state: RootState) : boolean => selectLoadingState(state) === LoadingState.LOADED;