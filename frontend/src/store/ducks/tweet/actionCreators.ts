import {LoadingState} from "./contracts/state";
import {Tweet} from "../tweets/contracts/state";
import {
    DataTweetActionsType,
    FetchTweetActionInterface,
    SetTweetDataActionInterface,
    SetTweetLoadingStateActionInterface,
} from "./contracts/actionTypes";


export const setTweetData = (payload: Tweet) : SetTweetDataActionInterface => ({
    type: DataTweetActionsType.SET_TWEET_DATA,
    payload
})

export const setTweetLoadingState = (payload: LoadingState) : SetTweetLoadingStateActionInterface => ({
    type: DataTweetActionsType.SET_LOADING_STATE,
    payload
})


export const fetchTweet = (payload: string) : FetchTweetActionInterface => ({
    type: DataTweetActionsType.FETCH_TWEET_DATA,
    payload
})


export type TweetActions =
    | SetTweetDataActionInterface
    | FetchTweetActionInterface
    | SetTweetLoadingStateActionInterface
