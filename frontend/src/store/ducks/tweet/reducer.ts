import produce, {Draft} from 'immer';
import {TweetState} from "../tweets/contracts/state";
import {DataTweetState, LoadingState} from "./contracts/state";
import {TweetActions} from "./actionCreators";
import {DataTweetActionsType} from "./contracts/actionTypes";

export const initialTweetState : DataTweetState = {
    data: undefined,
    loadingState: LoadingState.NEVER
}

export const tweetReducer = produce((draft: Draft<DataTweetState>, action: TweetActions) => {
    switch (action.type) {
        case DataTweetActionsType.SET_TWEET_DATA:
            draft.data = action.payload;
            draft.loadingState = LoadingState.LOADED;
            break;

        case DataTweetActionsType.FETCH_TWEET_DATA:
            draft.data = undefined;
            draft.loadingState = LoadingState.LOADING;
            break;

        case DataTweetActionsType.SET_LOADING_STATE:
            draft.loadingState = action.payload;
            break;

        default:
            break;
    }
}, initialTweetState)