import {Action} from "redux";
import {LoadingState} from "./state";
import {Tweet} from "../../tweets/contracts/state";

export enum DataTweetActionsType{
    SET_TWEET_DATA = 'tweet/SET_TWEET_DATA',
    FETCH_TWEET_DATA = 'tweet/FETCH_TWEET_DATA',
    SET_LOADING_STATE = 'tweet/SET_LOADING_STATE',
}

export interface SetTweetDataActionInterface extends Action<DataTweetActionsType>{
    type: DataTweetActionsType.SET_TWEET_DATA;
    payload: Tweet;
}


export interface SetTweetLoadingStateActionInterface extends Action<DataTweetActionsType>{
    type: DataTweetActionsType.SET_LOADING_STATE;
    payload: LoadingState;
}

export interface FetchTweetActionInterface extends Action<DataTweetActionsType>{
    type: DataTweetActionsType.FETCH_TWEET_DATA;
    payload: string
}

