import {call, put, takeLatest} from 'redux-saga/effects'
import {TagsApi} from "../../../services/api/tagsApi";
import {setTags} from "./actionCreators";
import {setTweetsLoadingState} from "../tweets/actionCreators";
import {TagsActionsType} from "./contracts/actionTypes";
import {LoadingState} from "./contracts/state";

export function* fetchTagsRequest() {
    try {
        // @ts-ignore
        const items = yield call(TagsApi.fetchTags);
        yield put(setTags(items));
    }catch (error){
        yield put(setTweetsLoadingState(LoadingState.ERROR));
        console.log(error)
    }
}


export function* tagsSaga() {
    yield takeLatest(TagsActionsType.FETCH_TAGS, fetchTagsRequest)
}