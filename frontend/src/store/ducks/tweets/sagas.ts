import {call, put, takeLatest} from 'redux-saga/effects'
import {addTweets, setAddFormState, setTweets, setTweetsLoadingState,} from "./actionCreators";
import {TweetsApi} from "../../../services/api/tweetsApi";
import {AddFormState, LoadingState} from "./contracts/state";
import {FetchAddTweetsActionInterface, TweetActionsType} from "./contracts/actionTypes";

export function* fetchTweetsRequest() {
    try {
        // @ts-ignore
        const items = yield call(TweetsApi.fetchTweets);
        yield put(setTweets(items));
    }catch (error){
        yield put(setTweetsLoadingState(LoadingState.ERROR));
        console.log(error)
    }
}

export function* fetchAddTweetsRequest({payload}: FetchAddTweetsActionInterface) {
    try {
        const data = {
            text: payload,
            user: {
                fullname: "Aibek Ibraev",
                username: "i5raev",
                avatarUrl: "https://www.w3schools.com/w3images/avatar2.png",
            }
        }
        // @ts-ignore
        const item = yield call(TweetsApi.addTweets, data);
        yield put(addTweets(item));
    }catch (error){
        yield put(setAddFormState(AddFormState.ERROR));
        console.log(error)
    }
}

export function* tweetsSaga() {
    yield takeLatest(TweetActionsType.FETCH_TWEETS, fetchTweetsRequest)
    yield takeLatest(TweetActionsType.FETCH_ADD_TWEETS, fetchAddTweetsRequest)
}