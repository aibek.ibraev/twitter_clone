import {RootState} from "../../store";
import {AddFormState, LoadingState, TweetState} from "./contracts/state";
import { createSelector } from 'reselect';

export const selectTweets = (state: RootState): TweetState => state.tweets;

export const selectTweetsItems = createSelector(selectTweets, (tweets) => tweets.items);

export const selectLoadingState = (state: RootState) : LoadingState => selectTweets(state).loadingState;

export const selectAddFormState = (state: RootState) : AddFormState => selectTweets(state).addFormState;

export const selectIsTweetLoading = (state: RootState) : boolean => selectLoadingState(state) === LoadingState.LOADING;

export const selectIsTweetLoaded = (state: RootState) : boolean => selectLoadingState(state) === LoadingState.LOADED;