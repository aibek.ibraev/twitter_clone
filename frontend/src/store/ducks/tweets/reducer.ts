import {AddFormState, LoadingState, TweetState} from "./contracts/state";
import produce, {Draft} from 'immer';
import {TweetsActions} from "./actionCreators";
import {TweetActionsType} from "./contracts/actionTypes";

export const initialTweetsState: TweetState = {
    items: [],
    addFormState: AddFormState.NEVER,
    loadingState: LoadingState.NEVER
}

export const tweetsReducer = produce((draft: Draft<TweetState>, action: TweetsActions) => {

    switch (action.type) {
        case TweetActionsType.SET_TWEETS:
            draft.items = action.payload;
            draft.loadingState = LoadingState.LOADED;
            break;

        case TweetActionsType.FETCH_TWEETS:
            draft.items = [];
            draft.loadingState = LoadingState.LOADING;
            break;

        case TweetActionsType.SET_ADD_FORM_STATE:
            draft.addFormState = action.payload;
            break;

        case TweetActionsType.FETCH_ADD_TWEETS:
            draft.addFormState= AddFormState.LOADING;
            break;

        case TweetActionsType.ADD_TWEETS:
            draft.items.push(action.payload);
            draft.addFormState= AddFormState.NEVER;
            break;

        case TweetActionsType.SET_LOADING_STATE:
            draft.loadingState = action.payload;
            break;

        default:
            break;
    }
}, initialTweetsState)