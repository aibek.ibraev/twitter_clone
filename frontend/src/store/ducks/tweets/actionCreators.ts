import {AddFormState, LoadingState, Tweet, TweetState} from "./contracts/state";

import {
    AddTweetsActionInterface,
    FetchAddTweetsActionInterface,
    FetchTweetsActionInterface,
    SetAddFormStateActionInterface,
    SetTweetsActionInterface,
    SetTweetsLoadingStateActionInterface,
    TweetActionsType
} from "./contracts/actionTypes";



export const setTweets = (payload: TweetState['items']) : SetTweetsActionInterface => ({
    type: TweetActionsType.SET_TWEETS,
    payload
})

export const setTweetsLoadingState = (payload: LoadingState) : SetTweetsLoadingStateActionInterface => ({
    type: TweetActionsType.SET_LOADING_STATE,
    payload
})

export const setAddFormState = (payload: AddFormState) : SetAddFormStateActionInterface => ({
    type: TweetActionsType.SET_ADD_FORM_STATE,
    payload
})

export const fetchTweets = () : FetchTweetsActionInterface => ({
    type: TweetActionsType.FETCH_TWEETS,
})


export const fetchAddTweets = (payload: string) : FetchAddTweetsActionInterface => ({
    type: TweetActionsType.FETCH_ADD_TWEETS,
    payload
})

export const addTweets = (payload: Tweet) : AddTweetsActionInterface => ({
    type: TweetActionsType.ADD_TWEETS,
    payload
})

export type TweetsActions =
    | SetTweetsActionInterface
    | SetTweetsLoadingStateActionInterface
    | FetchTweetsActionInterface
    | FetchAddTweetsActionInterface
    | AddTweetsActionInterface
    | SetAddFormStateActionInterface
