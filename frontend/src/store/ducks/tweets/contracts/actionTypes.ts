import {Action} from "redux";
import {AddFormState, LoadingState, Tweet, TweetState} from "./state";

export enum TweetActionsType{
    SET_TWEETS = 'tweets/SET_TWEETS',
    FETCH_ADD_TWEETS = 'tweets/FETCH_ADD_TWEETS',
    FETCH_TWEETS = 'tweets/FETCH_TWEETS',
    ADD_TWEETS = 'tweets/ADD_TWEETS',
    SET_LOADING_STATE = 'tweets/SET_LOADING_STATE',
    SET_ADD_FORM_STATE = 'tweets/SET_ADD_FORM_STATE',
}

export interface SetTweetsActionInterface extends Action<TweetActionsType>{
    type: TweetActionsType.SET_TWEETS;
    payload: TweetState['items'];
}

export interface FetchAddTweetsActionInterface extends Action<TweetActionsType>{
    type: TweetActionsType.FETCH_ADD_TWEETS;
    payload: string;
}

export interface AddTweetsActionInterface extends Action<TweetActionsType>{
    type: TweetActionsType.ADD_TWEETS;
    payload: Tweet;
}

export interface SetTweetsLoadingStateActionInterface extends Action<TweetActionsType>{
    type: TweetActionsType.SET_LOADING_STATE;
    payload: LoadingState;
}

export interface SetAddFormStateActionInterface extends Action<TweetActionsType>{
    type: TweetActionsType.SET_ADD_FORM_STATE;
    payload: AddFormState;
}

export interface FetchTweetsActionInterface extends Action<TweetActionsType>{
    type: TweetActionsType.FETCH_TWEETS;
}