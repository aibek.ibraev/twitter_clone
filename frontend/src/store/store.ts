import {compose, createStore,applyMiddleware} from "redux";
import {rootReducer} from "./rootReducer";
import createSagaMiddleware from 'redux-saga';
import rootSaga from "./rootSaga";
import {TweetState} from "./ducks/tweets/contracts/state";
import {TagsState} from "./ducks/tags/contracts/state";
import {DataTweetState} from "./ducks/tweet/contracts/state";

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}
const sagaMiddleware = createSagaMiddleware()

export interface RootState {
    tweets: TweetState;
    tags: TagsState;
    tweet: DataTweetState;
}

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

export const store = createStore(rootReducer,composeEnhancers(applyMiddleware(sagaMiddleware)))

sagaMiddleware.run(rootSaga)