import {body} from 'express-validator';

export const registerValidations = [
    body('email', 'Введите E-Mail')
        .isEmail()
        .withMessage('Неверный E-Mail'),
   body('fullname', 'Введите Имя')
        .isString(),
    body('username', 'Укажите логин')
        .isString(),
    body('password', 'Укажите пароль')
        .isString()
        .isLength({
            min: 8,
        })
        .withMessage('Допустимое количество символов в пароле от 8'),
];