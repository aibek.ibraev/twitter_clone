import passport = require('passport');
import {Strategy as LocalStrategy} from 'passport-local';

import {UserModel, UserModelInterface} from "../models/UserModel";
import {generateMD5} from "../utils/generateHash";

declare global {
    namespace Express {
        interface User extends UserModelInterface {
        }
    }
}

passport.use(new LocalStrategy(async (username, password, done): Promise<void> => {
        try{
            const user = await UserModel.findOne({$or: [{email: username}, {username}]}).exec();

            if(!user){
                return done(null, false);
            }

            if(user.password === generateMD5(password + process.env.SECRET_KEY)){
                return done(null, user);
            }else{
                return done(null, false);
            }
        }catch (error){
            return done(error, false);
        }
    }
))

passport.serializeUser((user: UserModelInterface, done) => {
    done(null, user._id);
});

passport.deserializeUser((id, done) => {
    UserModel.findById(id, (err, user) => {
        done(null, user);
    })
})

export {passport};

