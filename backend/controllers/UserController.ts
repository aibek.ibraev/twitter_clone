import express = require('express');
import {UserModel} from "../models/UserModel";
import jwt = require('jsonwebtoken');
import {validationResult} from "express-validator";
import {generateMD5} from "../utils/generateHash";
import {isValidObjectId} from "../utils/isValidObjectId";

class UserController {
    async index(_: any, res: express.Response): Promise <void> {
        try {
            const users = await UserModel.find({}).exec();
            res.json({
                status: 'success',
                data: users
            });
        }catch (error){
            res.status(500).json({
                status: 'error',
                message: JSON.stringify(error)
            })
        }
    }

    async show(req: express.Request, res: express.Response): Promise <void> {
        try {
            const userId = req.params.id

            if(!isValidObjectId(userId)){
                res.status(404).send()
                return;
            }
            const user = await UserModel.findById(userId).exec();

            res.status(200).json({
                status: 'success',
                data: user
            });
        }catch (error){
            res.status(500).json({
                status: 'error',
                message: JSON.stringify(error)
            })
        }
    }

    async create(req: express.Request, res: express.Response): Promise <void> {
        try {
            const errors = validationResult(req);
            const data = {
                email: req.body.email,
                username: req.body.username,
                fullname: req.body.fullname,
                password: generateMD5(req.body.password + process.env.SECRET_KEY),
                confirmHash: generateMD5(process.env.SECRET_KEY || Math.random().toString())
            };
            if(!errors.isEmpty()){
                res.status(400).json({status: 'error', message: errors.array()})
                return ;
            }

            const user = await UserModel.create(data);

            //TODO выдает 500 ошибку и письмо не высылается

            // sendEmail({
            //     emailFrom: "admin@twitter.com",
            //     emailTo: data.email,
            //     subject: "Подтверждение почты Twitter Clone Tutorial",
            //     html: `
            //             Для того чтобы подтвердить почту, перейдите по ссылке
            //             <a href="http://localhost:${process.env.PORT || 8000}/users/verify?hash=${data.confirmHash}"/>
            //            `,
            // },
            //     (err: Error | null) => {
            //     if(err){
            //         console.log(err)
            //         res.status(500).json({
            //             status: 'error',
            //             message: err,
            //         })
            //     }else{
            //         res.status(201).json({
            //             status: 'success',
            //             data: user
            //         });
            //     }
            // })
            res.status(201).json({
                status: 'success',
                data: user
            });

        }catch (error){
            res.json({
                status: 'error',
                message: error
            })
        }
    }

    async verify(req: express.Request, res: express.Response): Promise <void> {
        try {
            const hash = req.query.hash;

            if(!hash){
                res.status(400).send();
                return
            }

            const user = await UserModel.findOne({confirmHash: hash}).exec();

            if(user){
                user.confirmed = true;
                user.save()

                res.json({
                    status: 'success',
                    data: user
                });
            } else{
                res.status(404).send().json({status: 'error', message: 'Пользователь не найден'});
            }
        }catch (error){
            res.status(500).json({
                status: 'error',
                message: JSON.stringify(error)
            })
        }
    }

    async afterLogin(req: express.Request, res: express.Response): Promise <void> {
        try {
            res.json({
                status: 'success',
                data: {
                    ...req.user,
                    token: jwt.sign(req.user || {}, process.env.SECRET_KEY || 'Secret')
                }
            })
        }catch (error){
            res.status(500).json({
                status: 'error',
                message: JSON.stringify(error)
            })
        }
    }
}

export const UserCtrl = new UserController();